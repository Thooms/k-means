#include <stdio.h>
#include <stdlib.h>

/* Basic disjoint-set forests. The union is to be improved. 
   The root of the tree is pointing to itself. */

struct Node {
    int elem;
    struct Node * parent;
};

typedef struct Node Node;

Node singleton(int x)
{
    Node n = {x, &n};
    return n;
}

Node * find(Node * n)
{
    if (n->parent == n)
	return n;
    else
	return find(n->parent);
}

void unionn(Node * m, Node * n)
{
    Node * m_root = find(m);
    Node * n_root = find(n);
    m_root->parent = n_root;
}

/* Abstraction under the form of the partition of a set [1,n] of
   integers. */

struct Partition {
    size_t size;
    Node ** nodes; /* This contains the pointers to the nodes of the partition. */
};

int main(void)
{
    return 0;
}
